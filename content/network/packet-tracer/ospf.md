---
title: "Konfigurasi OSPF di Packet Tracer (Dynamic Routing) - Packet Tracer"
date: 2022-03-09T19:37:45+07:00
draft: false
---

# Konfigurasi OSPF di Packet Tracer (Dynamic Routing)

## Apa itu dynamic routing?

---

Dynamic Routing atau Routing Dinamis adalah teknik routing yang mampu membuat tabel routing secara otomatis dan real-time berdasarkan lalu lintas jaringan dan beberapa router yang saling terhubung. Routing Dinamis mengijinkan router untuk saling berbagi informasi network, dan mengijinkan router-router tersebut untuk memilih jalur terbaik ke tujuan/destination.

Jenis jenis protokol routing dinamis:

1. RIP	->	Routing Information Protokol
2. OSPF	->	Open Shortest Path First
3. IGRP	->	Internal Gateway Routing Protokol
4. EIGRP ->	Enhanced Interior Gateway Routing Protocol 
5. BGP	->	Border Gateway Protokol
6. IS-IS -> Intermediate System to Intermediate System 

## Pengertian OSPF (Open Shortest Path First)

---

Open Shortest Path First (OSPF) adalah sebuah protokol routing otomatis (Dynamic Routing) yang mampu menjaga, mengatur dan mendistribusikan informasi routing antar network mengikuti setiap perubahan jaringan secara dinamis. Pada OSPF dikenal sebuah istilah Autonomus System (AS) yaitu sebuah gabungan dari beberapa jaringan yang sifatnya routing dan memiliki kesamaan metode serta policy pengaturan network, yang semuanya dapat dikendalikan oleh network administrator. Dan memang kebanyakan fitur ini diguakan untuk management dalam skala jaringan yang sangat besar. Oleh karena itu untuk mempermudah penambahan informasi routing dan meminimalisir kesalahan distribusi informasi routing, maka OSPF bisa menjadi sebuah solusi.

OSPF termasuk di dalam kategori IGP (Interior Gateway Protocol) yang memiliki  kemapuan Link-State dan Alogaritma Djikstra yang jauh lebih efisien dibandingkan protokol IGP yang lain. Dalam operasinya OSPF menggunakan protokol sendiri yaitu protokol 89. [_Wikipedia_](https://id.wikipedia.org/wiki/OSPF)

## Konfigurasi OSPF di Packet Tracer

---

Topology

![Topology](/notes/image/ospf-topology.png)

### Konfigurasi IP Address dan vlan di setiap device

---

#### Router 0

	Router>enable 
	Router#configure terminal 
	Router(config)#inter fa1/0
	Router(config-if)#
	Router(config-if)#ip address 10.10.10.1 255.255.255.0
	Router(config-if)#no sh
	Router(config-if)#
	Router(config-if)#inter fa0/0
	Router(config-if)#ip address 20.20.20.1 255.255.255.0
	Router(config-if)#no sh
	Router(config-if)#
	Router(config-if)#inter se2/0
	Router(config-if)#ip address 1.1.1.1 255.255.255.252
	Router(config-if)#no sh

#### Router 1

	Router>enable 
	Router#configure terminal 
	Router(config)#inter se2/0
	Router(config-if)#ip address 1.1.1.2 255.255.255.252
	Router(config-if)#no sh
	Router(config-if)#
	Router(config-if)#exit
	Router(config)#inter se3/0
	Router(config-if)#ip address 2.2.2.1 255.255.255.252
	Router(config-if)#no sh

#### Router 2

	Router>enable
	Router#conf t
	Router(config)#inter se2/0
	Router(config-if)#ip address 2.2.2.2 255.255.255.252
	Router(config-if)#no sh
	Router(config-if)#
	Router(config-if)#inter fa0/0
	Router(config-if)#ip address 3.3.3.1 255.255.255.252
	Router(config-if)#no sh
	Router(config-if)#inter fa1/0
	Router(config-if)#ip address 4.4.4.1 255.255.255.252
	Router(config-if)#no sh
	Router(config-if)#

#### Server 0 dan server 1

Static ip

![server1](/notes/image/ospf-server0.png)

<p/>

![server1](/notes/image/ospf-server1.png)


#### Switch 0 dan Switch 1

Switch 0 dan switch 1 konfigurasi sama
	
	Switch>enable 
	Switch#conf t
	Switch(config)#vlan 10
	Switch(config-vlan)#name kelasA
	Switch(config-vlan)#vlan 20
	Switch(config-vlan)#name kelasB
	Switch(config-vlan)#exit
	Switch(config)#interface fa0/2
	Switch(config-if)#switchport access vlan 10
	Switch(config-if)#interface fa0/3
	Switch(config-if)#switchport access vlan 20
	Switch(config-if)#exit
	Switch(config)#
	Switch(config)#interface fa0/1
	Switch(config-if)#switchport mode trunk

#### Switch 2

	Switch>enable
	Switch#conf t
	Switch(config)#vlan 30
	Switch(config-vlan)#name kelasB
	Switch(config-vlan)#exit
	Switch(config)#interface fa0/2
	Switch(config-if)#switchport access vlan 30
	Switch(config-if)#interface fa0/3
	Switch(config-if)#switchport access vlan 30
	Switch(config-if)#interface fa0/1
	Switch(config-if)#switchport mode trunk	
	Switch(config-if)#

#### Multilayer Switch0

	Switch>enable
	Switch#conf t
	Switch(config)#ip routing
	Switch(config)#inter fa0/1
	Switch(config-if)#no switchport
	Switch(config-if)#ip address 3.3.3.2 255.255.255.252
	Switch(config-if)#

Membuat vlan id dan ip interface vlan

	Switch(config)#vlan 10
	Switch(config-vlan)#name kelasA
	Switch(config-vlan)#vlan 20
	Switch(config-vlan)#name kelasB
	Switch(config-vlan)#exit
	Switch(config)#interface vlan 10
	Switch(config-if)#ip address 192.168.10.1 255.255.255.248
	Switch(config-if)#interface vlan 20
	Switch(config-if)#ip address 192.168.20.1 255.255.255.248
	Switch(config-if)#

DHCP server

	Switch(config)#ip dhcp pool kelasA
	Switch(dhcp-config)#network 192.168.10.0 255.255.255.248
	Switch(dhcp-config)#default-router 192.168.10.1
	Switch(dhcp-config)#
	Switch(dhcp-config)#ip dhcp pool kelasB
	Switch(dhcp-config)#network 192.168.20.0 255.255.255.248
	Switch(dhcp-config)#default-router 192.168.20.1
	Switch(dhcp-config)#exit
	Switch(config)#ip dhcp excluded-address 192.168.10.1
	Switch(config)#ip dhcp excluded-address 192.168.20.1

Aktifkan dhcp client pada semua client yang terhubung dengan multilayer switch0

#### Multilayer Switch1

IP interface, ip vlan interface, enable ip routing
	
	Switch>enable
	Switch#config ter
	Switch(config)#interface fa0/1
	Switch(config-if)#no switchport
	Switch(config-if)#
	Switch(config-if)#ip address 4.4.4.2 255.255.255.252
	Switch(config-if)#
	Switch(config-if)#exit
	Switch(config-if)#
	Switch(config)#vlan 30
	Switch(config-vlan)#name kelasC
	Switch(config-vlan)#exit
	Switch(config)#interface vlan 30
	Switch(config-if)#ip address 192.168.30.1 255.255.255.0
	Switch(config-if)#exit
	Switch(config)#
	Switch(config)#ip routing

DHCP server

	Switch(config)#ip dhcp pool kelasC
	Switch(dhcp-config)#network 192.168.30.0 255.255.255.0
	Switch(dhcp-config)#default-router 192.168.30.1
	Switch(dhcp-config)#exit
	Switch(config)#ip dhcp excluded-address 192.168.30.1
	Switch(config)#

Aktifkan DHCP client di semua client yang terhubung dengan multilayer switch1

### Konfigurasi OSPF

Perintah:

"router ospf \<process id\>"<br/>
"network \<network address\> \<wildcard bits> area \<area>"

**wildcard bits([wikipedia](https://en.wikipedia.org/wiki/Wildcard_mask)):**<br/>
A wildcard mask can be thought of as an inverted subnet mask. For example, a subnet mask of 255.255.255.0 (binary equivalent = 11111111.11111111.11111111.00000000) inverts to a wildcard mask of 0.0.0.255 (binary equivalent = 00000000.00000000.00000000.11111111).<br>
A wild card mask is a matching rule.[2] The rule for a wildcard mask is:
- 0 means that the equivalent bit must match<br/>
- 1 means that the equivalent bit does not matter

Contoh konfigurasi: 

Process id = 10 <br/>
area = 0 

- #### Router 0

<p/>

	Router(config)#router ospf 10
	Router(config-router)#network 10.10.10.0 0.0.0.255 area 0
	Router(config-router)#network 20.20.20.0 0.0.0.255 area 0
	Router(config-router)#network 1.1.1.0 0.0.0.3 area 0

- #### Router 1

<p/>

	Router(config)#router ospf 10
	Router(config-router)#network 1.1.1.0 0.0.0.3 area 0
	Router(config-router)#network 2.2.2.0 0.0.0.3 area 0

- #### Router 2

<p/>

	Router(config)#router ospf 10
	Router(config-router)#network 2.2.2.0 0.0.0.3 area 0
	Router(config-router)#network 3.3.3.0 0.0.0.3 area 0
	Router(config-router)#network 4.4.4.0 0.0.0.3 area 0
	Router(config-router)#

- #### Multilayer Switch 0

<p/>

	Switch(config)#router ospf 10
	Switch(config-router)#network 3.3.3.0 0.0.0.3 area 0
	Switch(config-router)#network 192.168.10.0 0.0.0.7 area 0
	Switch(config-router)#network 192.168.20.0 0.0.0.7 area 0
	
- #### Multilayer Switch 1

<p/>

	Switch(config)#router ospf 10
	Switch(config-router)#network 4.4.4.0 0.0.0.3 area 0
	Switch(config-router)#network 192.168.30.0 0.0.0.255 area 0
	
### Pengujian

Semua device bisa saling terhubung atau ping/kirim pesan

![hasil](/notes/image/ospf-hasil.png)
