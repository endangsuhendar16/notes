---
title: "Voip Sederhana - Packet Tracer"
date: 2023-01-24T22:09:49+07:00
draft: false
---

# Konfigurasi Voice Over IP (VOIP) Sederhana - Packet Tracer

## Apa Itu VOIP?

---

Voice over Internet Protocol (juga disebut VoIP, IP Telephony, Internet telephony atau Digital Phone) adalah teknologi yang memungkinkan percakapan suara jarak jauh melalui media internet. Data suara diubah menjadi kode digital dan dialirkan melalui jaringan yang mengirimkan paket-paket data, dan bukan lewat sirkuit analog telepon biasa.

Definisi VoIP adalah suara yang dikirim melalui protokol internet (IP).

## Topology

---

![topology](/notes/image/voip-sederhana.png)

## Langkah - Langkah Konfigurasi VOIP

---

Beberapa poin langkah-langkah yang harus diperhatikan:

VOIP Client:

 - Hidupkan client device dengan memasang kabel power

Router:

 - Setting IP Address pada interface router
 - Setting DHCP server pada router
 - Call Manager telephony service
 - Mengalokasikan angka ke ip phones

Switch:

 - Switch port yang menuju voip client ke voice vlan

VOIP Client:

 - Testing memanggil antar client

### Hidupkan client device dengan memasang kabel power

Lakukan hal yang sama ke semua device client

![add power](/notes/image/add-power-voip.gif)

### Setting IP Address pada interface router

	Router>enable
	Router#configure terminal
	Router(config)#interface fa0/0
	Router(config-if)#ip address 192.168.1.1 255.255.255.240
	Router(config-if)#no shutdown
	Router(config-if)#exit

### Setting DHCP server pada router

	Router(config)#ip dhcp pool voip-dhcp
	Router(dhcp-config)#network 192.168.1.0 255.255.255.240
	Router(dhcp-config)#default-router 192.168.1.1
	Router(dhcp-config)#option 150 ip 192.168.1.1
	Router(dhcp-config)#exit

**Apa itu dhcp 150 (option 150)?** cisco ip phone download konfigurasi dari TFTP server. Ketika sebuah cisco ip phone di hidupkan, jika ip phone tersebut tidak mempunyai konfigurasi ip address dan TFTP server ip adddress, ip phone akan mengirim request dengan menggunakan option 150 ke dhcp server untuk mendapatkan konfigurasi tersebut.

**DHCP option 150 adalah cisco proprietary** IEEE standards yang mirip adalah option 66. Seperti halnya 150, option 66 biasanya digunakan untuk menentukan nama dari TFTP server.

### Call Manager telephony service

	Router(config)#telephony-service
	Router(config-telephony)#max-dn 5
	Router(config-telephony)#max-ephones 5
	Router(config-telephony)#ip source-address 192.168.1.1 port 2000
	Router(config-telephony)#auto assign 1 to 5
	Router(config-telephony)#exit

**max-dn 5** artinya maximum directory number berjumlah 5.<br/>
**max-ephones 5** artinya maximum ephones berjumlah 5.<br/>
**ip source-address 192.168.1.1 port 2000** artinya IP dan Port yang kita tentukan akan menjadi source atau sumber komunikasi voip. <br/>
**auto assign 1 to 5** artinya router akan memberikan nomor telepon otomatis ke client, yang dimana nomor telepon tersebut diambil dari Directory Number yang sudah dibuat.<br/>

### Mengalokasikan angka ke ip phones

	Router(config)#ephone-dn 1
	Router(config-ephone-dn)#number 5001
	Router(config-ephone-dn)#ephone-dn 2
	Router(config-ephone-dn)#number 5002
	Router(config-ephone-dn)#ephone-dn 3
	Router(config-ephone-dn)#number 5003
	Router(config-ephone-dn)#ephone-dn 4
	Router(config-ephone-dn)#number 5004
	Router(config-ephone-dn)#ephone-dn 5
	Router(config-ephone-dn)#number 5005
	Router(config-ephone-dn)#exit

### Switch port yang menuju voip client ke voice vlan

	Switch>
	Switch>enable
	Switch#configure terminal
	Switch(config)#interface range fa0/2-5
	Switch(config-if-range)#switchport mode access
	Switch(config-if-range)#switchport voice vlan 1
	Switch(config-if-range)#exit

### Testing memanggil antar client

Lakukan testing panggilan antar client

![testing](/notes/image/testing-voip.gif)

