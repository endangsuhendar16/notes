---
title: "Konfigurasi VTP (Virtual Trunking Protocol) - Packet Tracer"
date: 2023-01-20T19:37:07+07:00
draft: false
---

# Konfigurasi Virtual Trunking Protocol (VTP) di Cisco - Packet Tracer

## Apa itu VTP?

---

VTP adalah protokol proprietary Cisco yang menyebarkan definisi Virtual Local Area Network (VLAN) di seluruh jaringan area lokal. Untuk melakukan ini, VTP membawa informasi VLAN ke semua switch di domain VTP.

Dengan menggunakan VTP, konfigurasi VLAN hanya perlu dilakukan di satu switch saja. Secara otomatis penambahan dan perubahan VLAN akan advertise ke switch lainnya sehingga VLAN database di semua switch akan selalu konsisten.

VTP ada tiga mode : Server, Client dan Transparent

 - VTP Server : ketika switch tersebut membuat VLAN misalnya VLAN 10, maka secara otomatis vtp update akan dikirimkan ke semua switch, dan kemudian switch client secara otomatis akan memprosesnya sehingga pada switch client akan juga terdapat vlan 10.
 - Hal tersebut diinginkan agar semua vlan yang ada pada semua switch adalah konsisten, sama nilainya
 - VTP Server bisa membuat, mengubah, dan menghapus VLAN. Memforward VTP update serta memproses VTP update yang diterimanya
 - VTP transparent sifatnya independent, bila dia membuat atau mengubah VLAN maka hanya berlaku untuk switch tersebut saja, tidak diadvertise ke switch yang lain
 - VTP Transparent memforward semua VTP update yang diterimanya, namun tidak memproses ke dalam vlan databasenya, sifatnya hanya meneruskan saja
 - VTP Client tidak bisa membuat VLAN. Hanya memproses VLAN yang diterima melalui VTP update yang dikirimkan oleh VTP Server
 - Semua mode VTP apapun yang berada dalam VTP domain yang sama akan meneruskan VTP update yang diterimanya ke switch yang lain.

## Topology

---

![Topology](/notes/image/vtp-topology.png)

## Konfigurasi VTP Server dan Client di Switch

---

Pada percobaan kali ini, semua pc client telah di setting IP Address nya secara static

### Multilayer Switch0

Setting vlan id dan ip di setiap interface vlan

	Switch>enable
	Switch#configure terminal
	Switch(config)#vlan 10
	Switch(config-vlan)#name RUANG10
	Switch(config-vlan)#exit
	Switch(config)#vlan 20
	Switch(config-vlan)#name RUANG20
	Switch(config-vlan)#exit
	Switch(config)#vlan 99
	Switch(config-vlan)#name RUANG_TAMU
	Switch(config-vlan)#exit
	Switch(config)#interface vlan 10
	Switch(config-if)#ip address 192.168.10.1 255.255.255.248
	Switch(config-if)#interface vlan 20
	Switch(config-if)#ip address 192.168.20.1 255.255.255.248
	Switch(config-if)#exit
	Switch(config)#interface vlan 99
	Switch(config-if)#ip address 192.168.99.1 255.255.255.248
	Switch(config-if)#exit
	Switch(config)#

Hidupkan routing di multilayer switch dan aktifkan trunk mode

	Switch(config)#interface fa0/1
	Switch(config-if)#switchport trunk encapsulation dot1q
	Switch(config-if)#interface fa0/2
	Switch(config-if)#switchport trunk encapsulation dot1q
	Switch(config-if)#exit
	Switch(config)#
	Switch(config)#ip routing

Setting VTP Server

	Switch(config)#vtp mode server
	Switch(config)#vtp domain utama
	Switch(config)#vtp password utama
	Switch(config)#

### Switch 0 (Transparent)

Jika ingin menambahkan vlan di switch dengan VTP mode transparent, kita harus menambahkan vlan secara manual, karena VTP mode transparent sifatnya hanya meneruskan vlan dari server saja, tidak menambahkannya ke dalam vlan databasenya secara otomatis.

Setting trunk mode

	Switch>
	Switch>enable
	Switch#configure terminal
	Switch(config)#interface fa0/1
	Switch(config-if)#switchport mode trunk
	Switch(config-if)#exit
	Switch(config)#interface fa0/2
	Switch(config-if)#switchport mode trunk

Switch(config-if)#

Setting VTP dengan mode Transparent

	Switch(config)#vtp mode transparent
	Switch(config)#vtp domain utama
	Switch(config)#vtp password utama
	Switch(config)#

Setting vlan 10 dan vlan 99 secara manual

	Switch(config)#vlan 10
	Switch(config-vlan)#name RUANG10
	Switch(config-vlan)#vlan 99
	Switch(config-vlan)#name RUANG_TAMU
	Switch(config-vlan)#exit

Tambahkan interface yang terhubung ke client ke masing masing vlan sesuai topology

	Switch(config)#
	Switch(config)#interface fa0/4
	Switch(config-if)#switchport access vlan 10
	Switch(config-if)#interface fa0/3
	Switch(config-if)#switchport access vlan 99
	Switch(config-if)#exit
	Switch(config)#

### Switch 1 (Client)

Setting trunk mode

	Switch>
	Switch>enable
	Switch#configure terminal
	Switch(config)#interface fa0/1
	Switch(config-if)#switchport mode trunk
	Switch(config)#exit

Setting VTP ke client mode

	Switch(config)#vtp mode client
	Switch(config)#vtp domain utama
	Switch(config)#vtp password utama
	Switch(config)#

Maka, VLAN database akan ter-update secara otomatis dari VTP server

	Switch(config)#do sh vlan br

	VLAN Name                             Status    Ports
	---- -------------------------------- --------- -------------------------------
	1    default                          active    Fa0/2, Fa0/3, Fa0/4, Fa0/5
	                                                Fa0/6, Fa0/7, Fa0/8, Fa0/9
	                                                Fa0/10, Fa0/11, Fa0/12, Fa0/13
	                                                Fa0/14, Fa0/15, Fa0/16, Fa0/17
	                                                Fa0/18, Fa0/19, Fa0/20, Fa0/21
	                                                Fa0/22, Fa0/23, Fa0/24, Gig0/1
	                                                Gig0/2
	10   RUANG10                          active
	20   RUANG20                          active
	99   RUANG_TAMU                       active
	1002 fddi-default                     active
	1003 token-ring-default               active
	1004 fddinet-default                  active
	1005 trnet-default                    active
	Switch(config)#

Switch client port ke access vlan sesuai topology

	Switch(config)#interface
	Switch(config)#interface fa0/2
	Switch(config-if)#switchport access vlan 20
	Switch(config-if)#exit

### Switch 2 (Client)

Setting trunk mode

	Switch>
	Switch>enable
	Switch#configure terminal
	Enter configuration commands, one per line.  End with CNTL/Z.
	Switch(config)#interface fa0/1
	Switch(config-if)#switchport mode trunk
	Switch(config-if)#exit
	Switch(config)#

Setting VTP ke client mode

	Switch(config)#
	Switch(config)#vtp mode client
	Switch(config)#vtp domain utama
	Switch(config)#vtp password utama
	Switch(config)#

Maka, VLAN database akan ter-update secara otomatis dari VTP server

	Switch(config)#do sh vlan bri

	VLAN Name                             Status    Ports
	---- -------------------------------- --------- -------------------------------
	1    default                          active    Fa0/2, Fa0/3, Fa0/4, Fa0/5
	                                                Fa0/6, Fa0/7, Fa0/8, Fa0/9
	                                                Fa0/10, Fa0/11, Fa0/12, Fa0/13
	                                                Fa0/14, Fa0/15, Fa0/16, Fa0/17
	                                                Fa0/18, Fa0/19, Fa0/20, Fa0/21
	                                                Fa0/22, Fa0/23, Fa0/24, Gig0/1
	                                                Gig0/2
	10   RUANG10                          active
	20   RUANG20                          active
	99   RUANG_TAMU                       active
	1002 fddi-default                     active
	1003 token-ring-default               active
	1004 fddinet-default                  active
	1005 trnet-default                    active
	Switch(config)#

Switch client port ke vlan sesuai topology

	Switch(config)#interface fa0/2
	Switch(config-if)#switchport access vlan 20
	Switch(config-if)#

### Testing

Ping antar client dan pastikan semua nya dapat terhubung dengan baik

