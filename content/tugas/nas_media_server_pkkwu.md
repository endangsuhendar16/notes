# INSTALASI MEDIA SERVER DAN NAS (Penyelesaian Langkah - Langkah Installasi - PKKWU)

## INSTALL OPENMEDIAVAULT

install gnupg untuk keperluan enkripsi dari trusted signed repository

    apt update
    apt-get install --yes gnupg

install wget dan download openmediavault trusted signed repository

    apt install wget
    wget -O "/etc/apt/trusted.gpg.d/openmediavault-archive-keyring.asc" https://packages.openmediavault.org/public/archive.key

tambahkan key tadi yang sudah di download ke apt-key

    apt-key add "/etc/apt/trusted.gpg.d/openmediavault-archive-keyring.asc"

buka dan edit source list apt

    nano /etc/apt/sources.list

tambahkan baris berikut

    deb https://packages.openmediavault.org/public shaitan main

update repository

    apt update

jika output menampilkan conflict dependencies seperti di bawah 

    The following packages have unmet dependencies:
     chrony : Conflicts: time-daemon
     systemd-timesyncd : Conflicts: time-daemon
    E: Error, pkgProblemResolver::Resolve generated breaks, this may be caused by held packages.

install salah satu package tersebut

    apt install chrony

install openmediavault

    apt install openmediavault

untuk mengakses website  dan mengkonfigurasi openmediavault yang telah kita
install, buka web browser dan masuk ke ip address dari server, login menggunakan
user admin dan password openmediavault

    http://192.168.1.33

Note:

lokasi mounted disk ada di /srv/disk-nama-disk

## INSTALLASI JELLYFIN

install extrepo

    apt install extrepo

tambahkan jellyfin repository menggunakan extrepo

    extrepo enable jellyfin

update repository

    apt update

install jellyfin

    apt install jellyfin

untuk mengakses jellyfin yang telah kita install,
buka web browser dan masuk ke ip address dari server dan tambahkan port 8096

    http://192.168.1.33:8096


software => yt-dlp ffmpeg openssh-server sudo rtcwake
